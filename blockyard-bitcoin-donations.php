<?php
/**
 * Plugin Name: Bitcoin Donations by The Blockyard
 * Plugin URI: https://www.theblockyard.io
 * Description: Add a Bitcoin donations button to a site.
 * Version: 2019.05.13.1249
 * Author: The Blockyard
 * Author URI: https://www.theblockyard.io
 * License:           GPL-3.0+
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain:       blockyard-bitcoin-donations
 * Domain Path:       /languages
 */
defined( 'ABSPATH' ) || die;
